"""
Constants used in the Package
"""
# CFG Constants
CFG_GRID_LIMIT = 'grid_limit'
CFG_ESS_FEED_LIMIT = 'ess_feed_limit'
CFG_ESS_CHARGE_LIMIT = 'ess_charge_limit'

CFG_SOC_DISCHARGE_LIMIT = 'soc_discharge_limit'
CFG_SOC_CHARGE_LIMIT = 'soc_charge_limit'
CFG_BATTERY_CAPACITY = 'battery_capacity'